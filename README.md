# Robottle

- [Robottle](#robottle)
  - [Description](#description)
  - [Common package](#common-package)
  - [Account package](#account-package)
  - [Device package](#device-package)
  - [Collector package](#collector-package)
  - [Frontend](#frontend)
  - [Running](#running)
  - [TODO](#todo)

### Description

Robottle is a simple IoT platform written in Go.

Users can create accounts and add devices. Each device has a unique key and secret that should be
used to sign the payload sent by the devices.

Each service exposes a RESTful API that is accessed through a common API gateway.

### Common package

The [`robottle_common`](https://gitlab.com/robottle/robottle_common) package contains the common functionality used by other
services, like reading configuration, system connections (key/value store, message queue systems,
databases), authentication and hashing strategies, etc.

### Account package

The [`robottle_account`](https://gitlab.com/robottle/robottle_account) package manages the creation of account and users.
This package also creates and validates authentication tokens. 

### Device package

The [`robottle_device`](https://gitlab.com/robottle/robottle_device) package is used to create devices and validate the
payload sent by these devices.

### Collector package

The [`robottle_collector`](https://gitlab.com/robottle/robottle_collector) package exposes an API to send device data. This 
data is sent to a queue message system to then be validated and stored.

The collector service also exposes a stream server that sends device data as it is processed.
This stream method receives a the device account and sensor so it can be filtered by this field. This is
useful when one devices has multiple sensors. 

### Frontend

[`robottle_frontend`](https://gitlab.com/robottle/robottle_frontend) is a React application where users can sign up for an 
account and manage devices.

The idea is that a user can add devices and consume the data the device produces to build dashboards reading 
multiple device sensors.

The following is a simple image of data coming from the device data stream. Using the `robottle_client`
you can simulate getting weather information:

```bash
$ go run *.go simulate weather --key device_key --secret device_secret --city="Mexico City" 
```

This will pull weather information from [Open Weather API](https://openweathermap.org/api) and send new information
every two minutes. 

!["Robottle main UI"](robottle.png)

You will need to sign up for an API key and set `OPEN_WEATHER_API_KEY` with that value for this to work. 

### Running

The project uses `docker-compose` for development. Just run `docker-compose up` and everything should be working
after some seconds.

### TODO

- Improve frontend.
- Add pre-configured gauges.
